# -*- coding: utf-8 -*-

import sys,os.path,re,os
import easygui
import glob
import subprocess , shutil
import os, re
import win32com.client
from os.path import basename
from unidecode import unidecode
from random import shuffle
import unicodedata 

def remove_accents(s): 
    nkfd_form = unicodedata.normalize('NFKD', s) 
    return u''.join([c for c in nkfd_form if not unicodedata.combining(c)])

import configparser
config = configparser.ConfigParser()
config.read('RandomPlaylist.cfg')
updateConfigFile = False
""" if "PathDVDDecrypter" not in config:
    DVDDecrypterpath=subprocess.run(['where','DVDDecrypter'], stdout = subprocess.PIPE, stderr=subprocess.PIPE)
    if DVDDecrypterpath.returncode == 1:
       PathDVDDecrypter=easygui.diropenbox("Select DVDDecrypter directory ", "Directory DVDDecrypter")
"""
if 'PARAMETERS' not in config :
     QuietMode = False
     Filepostfix="mp3","ogg","flac","dts","m4A"

     config['PARAMETERS']={}
     config['PARAMETERS']['Filepostfix'] = '"mp3","ogg","flac","dts","m4A"'
     updateConfigFile = True
else:
    QuietMode = config['PARAMETERS'].getboolean('QuietMode')
    if QuietMode is None:
      config['PARAMETERS']['QuietMode'] = 'False'
      updateConfigFile = True
    Filepostfix =  config['PARAMETERS'].get('Filepostfix')
    if Filepostfix is None:
      config['PARAMETERS']['Filepostfix'] = '"mp3","ogg","flac","dts","m4A"'
      updateConfigFile = True
if updateConfigFile :     
    with open('RandomPlaylist.cfg', 'w') as configfile:
            config.write(configfile)    

Filepostfix =  [e.strip() for e in config['PARAMETERS'].get('Filepostfix').split(',')]
       
try:    
    #gather all link files
    linkargs = [ arg for arg in sys.argv[1:]  if '.' not in arg or os.path.splitext(os.path.basename(arg))[1] == '.lnk' ]
    #retrieve targets from links
    linkstringargument =[ win32com.client.Dispatch("WScript.Shell").CreateShortCut(arg).targetpath for arg in linkargs ]
    #gather other selection
    stringargument = [ arg for arg in sys.argv[1:]  if '.' not in arg or os.path.splitext(os.path.basename(arg))[1] != '.lnk' ]+linkstringargument    
    
    #delete nnon existing directories
    Add2Playlist =[ arg for arg in stringargument if os.path.exists(arg) ]
    
    if len(sys.argv[1:])==1 and os.path.isdir(sys.argv[1]):    
        playlistFile=sys.argv[1]+'\\'+os.path.basename(sys.argv[1]+'.m3u')
    else:
        #CommonPath='\\'.join(Add2Playlist[0].split("\\")[:-1])
        CommonPath=os.path.commonpath(stringargument)
        playlistFile=os.path.join(CommonPath,os.path.os.path.basename(CommonPath)+'.m3u')
    if  QuietMode: 
        print("Creating playlist %s" % (playlistFile))  
    else: 
        easygui.filesavebox('Name of m3u destination file','2playlist',default=playlistFile)
        #easygui.msgbox('Create a playlist for '+ ' '.join(Add2Playlist),'Make a m3u playlist')
    
    shellcmd='dir/b/s ' #dos shell command to gather 
    Allfiles=[]
    ErrorFileName=[]
    for loc in Add2Playlist:
        if os.path.isdir(loc):
            for ext in Filepostfix:
                linecmd=shellcmd+" "+'"'+loc+"\\*."+ext+'"'
                try:
                    FileListResult=subprocess.check_output(linecmd,shell=True)
                    FileList=FileListResult.splitlines()
                    for line in FileList :
                      if line:
                        try:
                            Allfiles.append(line.decode("utf8"))
                        except:
                             print("Error with file %s"%(line))
                             print('New file name will be %s'  %(line.decode("utf8",'replace')))
                             ErrorFileName.append(str(line))
                             
                             
                except:
                    easygui.exceptionbox()  
        else:
          Allfiles.append('"'+loc+'"')
            
    for i in range(10):
        shuffle(Allfiles)
        
    if ErrorFileName:
        raise Exception("Errors occurs on non UTF8 file name(s):\n%s" % ("\n".join(ErrorFileName)))  
    
    f = open(playlistFile, 'w+')
    f.writelines("\n".join(Allfiles))
    f.close()
    """MPCCommande='"C:\Program Files\MultiMedia\Video\MPC-HC\mpc-hc64.exe" + playlistFile+ "/play"'
    prompt=subprocess.check_output(MPCCommande,shell=True)"""
except :
     easygui.exceptionbox()  
else:
    if QuietMode:    
       print (" Terminated ok :o,  ")
    else:
       easygui.msgbox(" Terminated ok :o,  ")